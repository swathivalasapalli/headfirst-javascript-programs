let passengers = [
  { name: 'swathi', paid: true },
  { name: 'radhika', paid: true },
  { name: 'ramya', paid: false },
  { name: 'sirisha', paid: true }
];
function checkPaid(passengers) {
  for (let i = 0; i < passengers.length; ++i) {
    if (passengers[i].paid) {
      return false;
    }
  }
  return true;
}

function printPassengers(passengers) {
  for (let i = 0; i < passengers.length; ++i) {
    console.log(passengers[i].name);
    return false;
  }
  return true;
}

function processPassengers(passengers, testFunction) {
  for (let i = 0; i < passengers.length; ++i) {
    if (testFunction(passengers[i])) {
      return false;
    }
  }
  return true;
}

function checkNoFlyList(passenger) {
  return passenger.name === 'radhika';
}

function checkNotPaid(passenger) {
  return !passenger.paid;
}

var allCanFly = processPassengers(passengers, checkNoFlyList);
if (!allCanFly) {
  console.log('the plane cant take off:we have passenger on no fly list');
}
var allPaid = processPassengers(passengers, checkNotPaid);
if (!allPaid) {
  console.log('the plane cant take off, not everyone has paid');
}

console.log(printPassengers(passengers));
console.log(checkNoFlyList(passengers));
console.log(checkNotPaid(passengers));
console.log(passengers);
console.log(checkPaid(passengers));
