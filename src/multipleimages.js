const imagesHandler = () => {
  let images = document.getElementsByTagName('img');
  for (let i = 0; i < images.length; ++i) {
    images[i].onclick = showImage;
  }
};
window.onload = imagesHandler;
const showImage = eventObj => {
  let image = eventObj.target;
  let name = image.id;
  name = name + '.jpg';
  image.src = name;
};
