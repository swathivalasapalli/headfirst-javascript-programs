const imageReveal = () => {
  let images = document.getElementsByTagName('img');
  for (let i = 0; i < images.length; i++) {
    images[i].onclick = revealImage;
    images[i].onmouseover = revealImage;
    images[i].onmouseout = reblur;
  }
};
window.onload = imageReveal;
const revealImage = eventObj => {
  let image = eventObj.target;
  let name = image.id;
  name = name + '.jpg';
  image.src = name;
  setTimeout(reblur, 2000, image);
};

const reblur = eventObj => {
  let image = eventObj.target;
  let name = image.id;
  name = name + 'blur.jpg';
  image.src = name;
};
