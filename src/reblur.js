window.onload = function() {
  let images = document.getElementsByTagName('img');
  for (var i = 0; i < images.length; i++) {
    images[i].onclick = showAnswer;
  }
};

const showAnswer = eventObj => {
  var image = eventObj.target;
  var name = image.id;
  name = name + '.jpg';
  image.src = name;
  setTimeout(reblur, 2000, image);
};

const reblur = image => {
  var name = image.id;
  name = name + 'blur.jpg';
  image.src = name;
};
