var hello = 'hello globalvariable';
function whereAreYou() {
  var hello = 'hello localvariable';
  return hello;
}

var result = whereAreYou();
console.log(result);
console.log(hello);

var hai = 'global variable';
function nestedScope() {
  var hai = 'local varibale';
  function inner() {
    return hai;
  }
  return inner();
}

var re = nestedScope();
console.log(re);
console.log(hai);

var world = 'global variable';
function lexicalScope() {
  var world = 'global variable';
  function inner() {
    return world;
  }
  return inner;
}
var innerFunction = lexicalScope();
var r = innerFunction();
console.log(r);
console.log(world);

var count = 0;
function counter() {
  count = count + 1;
  return count;
}

console.log(counter());
console.log(counter());
console.log(counter());

function makeCounter() {
  var count = 1;
  function counter() {
    count += 1;
    return count;
  }
  return counter;
}
var doCount = makeCounter();
console.log(doCount());
console.log(doCount());
console.log(doCount());

function setTimer(doneMessage, n) {
  setTimeout(function() {
    console.log(doneMessage);
  }, n);
  doneMessage = 'OUCH!';
}
setTimer('Cookies are done!', 1000);

var secret = '007';
function getSecret() {
  var secret = '008';
  function getValue() {
    return secret;
  }
  return getValue();
}
console.log(getSecret());
console.log(secret);

window.onload = function() {
  var button = document.getElementById('bake');
  button.onclick = function() {
    console.log('Time to bake the cookies.');
    cookies.bake(2500);
  };
};
var cookies = {
  instructions: 'Preheat oven to 350...', bake: function(time) {
    console.log('Baking the cookies.');
    setTimeout(done, time);
  }
};
function done() {
  alert('Cookies are ready, take them out to cool.'); console.log('Cooling the cookies.');
  setTimeout(function() {
    alert('cookies are cool');
  }, 1000);
}

function multN(n) {
  return function multBy(m) {
    return n * m;
  };
}

var multBy3 = multN(3);
console.log('multiplying2:' + multBy3(2));
console.log('multiplying 3:' + multBy3(3));

function makeCounters() {
  var count = 0;
  return {
    increment: function() {
      count++;
      return count;
    }
  };
}
var c = makeCounters();
console.log(c.increment());
console.log(c.increment());
console.log(c.increment());

var s = '007';
function getS() {
  var s = '008';
  function getValue() {
    return s;
  }
  return getValue;
}
var getValueFun = getS();
console.log(getValueFun());
console.log(s);

var eat = function(food) {
  if (food === 'cookies') {
    console.log('more please');
  } else if (food === 'cake') {
    console.log('yummy yummy');
  }
};

console.log(eat('cookies'));
console.log(eat('cake'));

function Dog(name, breed, weight) {
  this.name = name;
  this.breed = breed;
  this.weight = weight;
}
var fido = new Dog('fido', 'mixed', 39);
var fluffy = new Dog('fluffy', 'poodle', 30);
var spot = new Dog('spot', 'chihua', 14);
var dogs = [fido, fluffy, spot];
for (var i = 0; i < dogs.length; i++) {
  var size = 'small';
  if (dogs[i].weight > 10) {
    size = 'large';
  }
  console.log('Dog: ' + dogs[i].name + ' is a ' + size + ' ' + dogs[i].breed);
}

function Dogg(name, breed, weight) {
  this.name = name;
  this.breed = breed;
  this.weight = weight;
  this.bark = function() {
    if (this.weight >= 25) {
      console.log(this.name + 'says Woof');
    } else {
      console.log(this.name + 'says fip');
    }
  };
}

var fidoo = new Dogg('fido', 'mixed', 12);
var fluffyy = new Dogg('fluffy', 'foodie', 28);
console.log(fidoo.bark());
console.log(fluffyy.bark());

function Coffee(roast, ounces) {
  this.roast = roast;
  this.ounces = ounces;
  this.getSize = function() {
    if (this.ounces === 8) {
      return 'small';
    } else if (this.ounces === 12) {
      return 'medium';
    } else if (this.ounces === 16) {
      return 'large';
    }
  };
  this.toString = function() {
    return 'you ordered a ' + this.getSize() + '' + this.roast + 'coffee';
  };
}

var houseBlend = new Coffee('Houseblend', 12);
console.log(houseBlend.toString());

var darkRoast = new Coffee('Darkroast', 16);
console.log(darkRoast.toString());

function Car(make, model, year, color, passenger, convertible, milege) {
  this.make = make;
  this.model = model;
  this.year = year;
  this.color = color;
  this.passenger = passenger;
  this.milege = milege;
  this.convertible = convertible;
  this.started = false;
  this.start = function() {
    this.started = true;
  };
}
var cars = new Car('GM', 'cadillac', 1955, 'tan', 5, false, 12345);
console.log(cars);

var cardiParams = {
  make: 'GM',
  model: 'cadillac',
  year: 1955,
  color: 'tan',
  passenger: 5,
  convertible: false,
  milege: 12892
};

function Carr(params) {
  this.make = params.make;
  this.model = params.model;
  this.year = params.year;
  this.color = params.color;
  this.passengers = params.passengers;
  this.convertible = params.convertible;
  this.milege = params.milege;
  this.started = false;
  this.start = function() {
    this.started = true;
  };
  this.stop = function() {
    this.started = false;
  };
  this.drive = function() {
    if (this.started) {
      console.log('zoom zoom');
    } else {
      console.log('you need to start the engine first');
    }
  };
}

var cadi = new Carr(cardiParams);
console.log(cadi.start());
console.log(cadi.drive());
console.log(cadi.stop());
console.log(cadi.drive());

var limoParams = {
  make: 'webville motors',
  model: 'line',
  year: 1983,
  color: 'black',
  passengers: 12,
  convertile: true,
  milege: 21120
};

var limo = new Carr(limoParams);
var limoDog = new Dog('rhapsody in blue', 'foodle', 40);
console.log(limo.make + '' + limo.model + 'is a' + typeof limo);
console.log(limoDog.name + 'is a' + typeof limoDog);

if (cadi instanceof Carr) {
  console.log('it is a car');
}

function Cat(name, breed, weight) {
  this.name = name;
  this.breed = breed;
  this.weight = weight;
}
var meow = new Cat('meow', 'siamese', 10);
var whiskers = new Cat('whiskers', 'mixed', 12);
var fiddo = { name: 'fido', breed: 'mixed', weight: 38 };

function dogCatcher(obj) {
  if (obj instanceof Doggie) {
    return true;
  }
  return false;
}
function Doggie(name, breed, weight) {
  this.name = name;
  this.breed = breed;
  this.weight = weight;
  this.bark = function() {
    if (this.weight > 25) {
      console.log(this.name + 'says Woof');
    } else {
      console.log(this.name + 'says Yip');
    }
  };
}
var flufy = new Doggie('fluffy', 'poodle', 30);
var spott = new Doggie('spot', 'chihuhuua', 17);
var dogss = [meow, whiskers, fiddo, flufy, spott];
for (var k = 0; k < dogss.length; ++k) {
  if (dogCatcher(dogss[k])) {
    console.log(dogss[k].name + 'is a dog');
  }
}

var f = new Dog('fido', 'mixed', 30);
f.owner = 'xyz';
console.log(f);
delete f.weight;
console.log(f);

var now = new Date();
var dateString = now.toString();
var year = now.getFullYear();
var theDayOfWeek = now.getDay();
console.log(dateString);
console.log(year);
console.log(theDayOfWeek);

var oddNumbers = new Array(3);
oddNumbers[0] = 1;
oddNumbers[1] = 3;
oddNumbers[2] = 5;

console.log(oddNumbers.reverse());
var aString = oddNumbers.join('-');
console.log(aString);
var areAllOdd = oddNumbers.every(function(x) {
  return x % 2 !== 0;
});
console.log(areAllOdd);
