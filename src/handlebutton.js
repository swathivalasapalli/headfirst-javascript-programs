var count = 0;
window.onload = function() {
  var button = document.getElementById('click');
  button.onclick = handleClick;
};

function handleClick() {
  var message = 'you clicked me';
  var div = document.getElementById('message');
  count += 1;
  div.innerHTML = message + count + 'times';
}
