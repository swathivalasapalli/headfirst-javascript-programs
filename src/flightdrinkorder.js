let passengers = [
  { name: 'swathi', paid: true, ticket: 'coach' },
  { name: 'radhika', paid: true, ticket: 'firstclass' },
  { name: 'ramya', paid: false, ticket: 'firstclass' },
  { name: 'sirisha', paid: true, ticket: 'coach' }
];
function createDrinkOrder(passenger) {
  let orderFunction;
  if (passenger.ticket === 'firstclass') {
    orderFunction = function() {
      console.log('would u like a cocktail or wine');
    };
  }
  orderFunction = function() {
    console.log('ur choice is cola or water');
  };

  return orderFunction;
}

function serveCustomer(passenger) {
  let getDrinkOrderFunction = createDrinkOrder(passenger);
  getDrinkOrderFunction();
  getDrinkOrderFunction();
  getDrinkOrderFunction();
  getDrinkOrderFunction();
}

function servePassenger(passengers) {
  for (let i = 0; i < passengers.length; ++i) {
    serveCustomer(passengers[i]);
  }
}

servePassenger(passengers);
