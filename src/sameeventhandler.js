const imageHandler = () => {
  let image0 = document.getElementById('zero');
  image0.onclick = showImageZero;
  let image1 = document.getElementById('one');
  image1.onclick = showImageOne;
  let image2 = document.getElementById('two');
  image2.onclick = showImageTwo;
};
window.onload = imageHandler;
const showImageZero = () => {
  var image = document.getElementById('zero');
  image.src = 'zero.jpg';
};
const showImageOne = () => {
  var image = document.getElementById('one');
  image.src = 'one.jpg';
};
const showImageTwo = () => {
  var image = document.getElementById('two');
  image.src = 'two.jpg';
};
